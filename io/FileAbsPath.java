import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileAbsPath {
    public static void main(String[] args) throws IOException {
        if (args.length < 1) {
            System.out.println ("Usage: FileAbsPath file");
            System.exit(-1);
        }
        
        Path inputPath = Paths.get(args[0]);
        System.out.format("FullPath: %s%n", inputPath.toAbsolutePath());
        System.out.format("URI: %s%n", inputPath.toUri());
        System.out.format("RealPath: %s%n", inputPath.toRealPath());
    }
}