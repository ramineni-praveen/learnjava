import java.nio.file.*;
import java.nio.file.attribute.*;
import java.io.IOException;

/**
 * Example utility that works like the df(1M) program to print out disk space
 * information.
 * https://docs.oracle.com/javase/tutorial/displayCode.html?code=https://docs.oracle.com/javase/tutorial/essential/io/examples/DiskUsage.java
 */

public class DiskUsage {
        static final long k = 1024;
        static void printFileStore(FileStore store) throws IOException {
            long total = store.getTotalSpace() / k;
            long used = (store.getTotalSpace() - store.getUnallocatedSpace()) / k;
            long avail = store.getUsableSpace() / k;
            
            String s = store.toString();
            if (s.length() > 20) {
                System.out.println(s);
                s = "";
            }
            System.out.format("%-20s %12d %12d %12d\n", s, total, used, avail);
        }
    
        public static void main(String[] args) throws IOException {
            System.out.format("%-20s %12s %12s %12s\n", "FileSystem", "kbytes", "used", "avail");            
            if (args.length == 0) {
                FileSystem fs = FileSystems.getDefault();
                for (FileStore store: fs.getFileStores()) {
                    printFileStore(store);
                }
            } else {
                for (String file: args) {
                    FileStore store = Files.getFileStore(Paths.get(file));
                    printFileStore(store);
                }
            }
        
        }
}