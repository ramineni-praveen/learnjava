package practice.java.util;

import java.util.List;

public class Song {
    private int track;
    private String title;
    
    public Song (int Track, String Title) {
        this.title = Title;
        this.track = Track;
    }
    
    public String getTitle() {
        return title;
    }
    
    public void setTitle(String Title) {
        this.title = Title;
    }
    
    public int getTrack() {
        return track;
    }
    
    public void setTrack(int Track) {
        this.track = Track;
    }
}