package practice.java.util;

import java.util.List;

public class Album {
    private String artist;
	private String title;
	private int year;
	private List<Song> songs;
	
	public Album(String title, String artist, int year) {
	    this.title = title;
	    this.artist = artist;
	    this.year = year;
	}
	
	public String getTitle() {
	    return title;
	}
	
	public String getArtist() {
	    return artist;
	}
	
	public int getYear() {
	    return year;
	}
	
	public void setTitle(String Title) {
	    this.title = Title;
	}
	
	public void setArtist(String Artist) {
	    this.artist = Artist;
	}
	
	public void setYear(int Year) {
	    this.year = Year;
	}
	
	public List<Song> getSongs() {
	    return this.songs;
	}
	
	public void setSongs(List<Song> Songs) {
	    this.songs = Songs;
	}
	
	@Override
	public String toString() {
	    return "Album {" +
                "artist='" + artist + '\'' +
				", title='" + title + '\'' +
				", year=" + year +
				'}';
	}
}